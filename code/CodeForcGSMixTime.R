# install.packages("sparseMVN")
require("sparseMVN")
# install.packages("Matrix")
require(Matrix)

### FUNCTIONS TO COMPUTE MIXING TIME OF cGS ####
###
Compute_A_cGS_matrix<-function(ii,sigma,tau_e){
  tau<-1/(sigma^2)
  nn<-apply(ii,2,max)
  d<-sum(nn)
  k<-length(nn)
  ind_blocks<-lapply(X =c(1:k) ,FUN = function(s){
    sum(nn[(1:s)][-s])+1:nn[s]
  })
  #### CREATE COUNTS
  N_s<-lapply(c(1:k),function(s){table(ii[,s])})
  #### CREATE PRIOR-POSTERIOR RATIOS
  r_s<-lapply(c(1:k),function(s){tau_e*N_s[[s]]/(tau_e*N_s[[s]]+tau[s])})
  #### CREATE CROSS COUNTS
  # print("creating cross counts")
  N_sl<-matrix(list(), nrow=k, ncol=k)
  for (s in 1:k){
    for (l in c(1:k)[-s]){
      N_sl[[s,l]]<-Matrix(c(table(ii[,s],ii[,l])), nrow = nn[s], ncol = nn[l], sparse = TRUE)
    }
  }
  #### CREATE A-MATRIX
  print("creating A_cGS matrix")
  # A_cGS<-Matrix(0, nrow = d, ncol = d, sparse = TRUE)
  A_cGS<-Matrix(rnorm(d^2), nrow = d, ncol = d, sparse = TRUE)
  for (s in 1:k){### HERE COULD AVOID FOR CYCLE AND MAKE THIS IN PARALLEL
    for (l in c(1:k)){
      if(s==l){
        A_cGS[ind_blocks[[s]],ind_blocks[[l]]]<-0
      }else{
        term_i_l<-vapply(c(1:nn[l]),function(i_l){ sum(r_s[[s]]*N_sl[[s,l]][,i_l]/N_s[[s]]   )/sum(r_s[[s]])},FUN.VALUE = 1)
        A_cGS[ind_blocks[[s]],ind_blocks[[l]]]<-### THIS INDICES EXTRACTION MAKES IT SOMEHOW SLOW!!
          -matrix(r_s[[s]]/N_s[[s]],nrow=nn[s],ncol=nn[l])*N_sl[[s,l]]+
          matrix(r_s[[s]],ncol=1)%*%matrix(term_i_l,nrow=1,ncol=nn[l])
      }
    }
  }
  return(A_cGS)
}
Compute_cGS_rate<-function(ii,sigma,tau_e,thresh=1e-5,eigen=FALSE){
  A<-Compute_A_cGS_matrix(ii,sigma,tau_e)
  B<-Compute_B_matrix(A)
  if(eigen){
    print("using eigen() rather than power method")
    rho<-max(abs(eigen(B)$values))    
  }else{
    rho<-power_meth(M=B, thresh=thresh)
  }
  return(rho)
}
Compute_cGS_mix_time<-function(ii,sigma,tau_e){
  return(1/(1-Compute_cGS_rate(ii,sigma,tau_e)))
}
Compute_B_matrix<-function(A){
  L<-tril(A)
  U<-triu(A)
  ImL <- L*-1
  diag(ImL)<-1
  B<-solve(ImL)%*%U
  return(B)
}  
power_meth = function(M, x0=NULL,blocks_size=NULL, thresh){
  print("Doing power method")
  if (is.null(x0)){
    if(is.null(blocks_size)){
      x0<-rnorm(n=dim(M)[1])
    }else{
      x0<-rep(rnorm(length(blocks_size)),times=blocks_size)+rnorm(sum(blocks_size),sd = 0.01)
    }
  }
  m0 = max(abs(x0))
  m0_vec<-m0
  x1 = M %*% matrix(x0 / m0,ncol=1)#(x0 / m0)#
  m1 = max(abs(x1))
  # while(abs(1/(1-m1) - 1/(1-m0)) > thresh){
  #   print(m0)
  #   m0 = m1
  #   x1 = M %*% (x1 / m1)
  #   m1 = max(abs(x1))
  #   m0_vec<-c(m0_vec,m1)
  # }
  for (iter in 1:10^4){
    if(abs(1/(1-m1) - 1/(1-m0)) < thresh){break}
    # print(m0)
    m0 = m1
    x1 = M %*% (x1 / m1)
    m1 = max(abs(x1))
    m0_vec<-c(m0_vec,m1)
  }
  # plot(1/(1-m0_vec))
  return(m1)
}

### EXAMPLE 1: FULL DESIGN
###
I<-100
J<-100
N<-I*J
y<-rep(0,N)
ii<-cbind(
  rep(1:I,each=J),rep(1:J,times=I)
)
### SET PRECISIONS
sigma<-c(1,1)
tau<-1/(sigma^2)
tau_e<-1
### COMPUTE mixing time of collapsed gibbs
cGS_mix_time<-Compute_cGS_mix_time(ii,sigma,tau_e)

## EXAMPLE 2: BALANCED LEVELS NON-FULL DESIGN
K<-5
I_vec<-c(6,rep(3,K-1)) ## (I_1,I_2,...I_K)
d_min<-3
N<-max(I_vec)*d_min
y<-rep(0,N)
ii<-NULL
# order_ind<-vector(mode = "list",length = K)
for (j in 1:d_min){
  new_rows<-matrix(NA,nrow = max(I_vec),ncol = K)
  for (k in 1:K){
    new_rows[,k]<-sample.int(I_vec[k])
  }
  ii<-rbind(ii,new_rows)
}
### SET PRECISIONS
sigma<-rep(1,K)
tau<-1/(sigma^2)
tau_e<-1
### COMPUTE mixing time of collapsed gibbs
cGS_mix_time<-Compute_cGS_mix_time(ii,sigma,tau_e)
