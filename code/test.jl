using Statistics 
using DataFrames
import DataFrames.DataFrame
using Distributions
using Random
using LinearAlgebra
using CSV
using FreqTables
using ProfileView
using SparseArrays
using ExtractMacro
#using Plots

include("functions.jl")

df = CSV.read("dataset-29789.csv") 
data = MyData(df[:,[1,2,3,4,6,7]]);
iter = 500
mu = zeros(Float64,iter)
tau_e = zeros(Float64,iter)
a = Array{ Array{Array{Float64,1},1},1}(undef,iter)
tau =  Array{Array{Float64,1},1}(undef, iter)
good = zeros(Int64,0)

@time Threads.@threads for t in 1:iter
    try
        println(t)
        mu[t], tau_e[t], a[t], tau[t] = MCMC_CrossedEffects_coupled(data, 100)
        push!(good,t)
    catch
        println("coupling not reached")
    end
end
open("results.txt", "w") do io
    print("finished chains ")
    println(io, len(good))
    print("mean tau_e ")
    println(io, mean(tau_e[good]))
    print("mean mu ")
    println(io, mean(mu[good]))
    print("mean tau ")
    println(io, mean(tau[good]))
    println(io, mean(a[good]))
end

len(good)


