using Statistics 
using DataFrames
import DataFrames.DataFrame
using Distributions
using Random
using LinearAlgebra
using CSV
using FreqTables
using ProfileView
using SparseArrays
using ExtractMacro
using Plots

function sum_on_all(a::Array{Array{Float64,1},1}, N_sl::Array{SparseMatrixCSC{Int64,Int64},2},is::Array{Int64,1}, k::Int64, N_s::Array{Array{Int64,1},1})
    #limitiamo il numero di liste 
    res = Array{Float64,1}(undef,is[k])
    K = size(a,1)
    
    res = zeros(Float64,is[k])
    for l in collect(1:K)[1:K .!= k]
        res .+= N_sl[k,l]*a[l]
    end
    for j in 1:is[k]
        if N_s[k][j] != 0
            res[j] = res[j]/ N_s[k][j]
        else
            res[j] = 0
        end
    end
    return res
end
    
function countmemb(itr)
    d = zeros(Int64, maximum(itr))
    
        
    for val in itr
        d[val] += 1
    end
    return d
end

struct MyData
    y::Array{Int64,1};
    ii::DataFrame;
    N::Int64;
    K::Int64;
    is::Array{Int64,1};
    N_s::Array{Array{Int64,1},1};
    mean_y_s::Array{Array{Float64,1},1};
    N_sl::Array{SparseMatrixCSC{Int64,Int64},2};
    
    function MyData(df::DataFrame)
        y = df.y
        ii = df[:, 1:5]
       
        #substitute zero value in column 5
        #ii.service[ii.service.== 0] .= 2;
        
        N = size(y,1)
        K = size(ii,2) 

        mean_y = mean(y)
        is = [maximum(ii[:,j]) for j in 1:K] #it will be the dimension of a_k
        N_s = [countmemb(ii[:,k]) for k in 1:K] #it automatically include the zeros i'm looking at 
        #it will have nan for values not interestin
        mean_y_s = [[mean(y[ii[:,k] .== h]) for h in 1:is[k]] for k in 1:K]

        for k in 1:K
            for i = eachindex(mean_y_s[k])
                if isnan(mean_y_s[k][i])
                    mean_y_s[k][i] = 0
                end
            end
        end
        
        
        # so now i know that whenever iterating, i do it on k = 1:is[k] inside indices[k]
        # Matrice di kxk

        #not THE fastest, but does what needed 
        N_sl= Array{SparseMatrixCSC{Int64,Int64},2}(undef, K,K)
     
        for j in 1:K
            for i in 1:K
                N_sl[i,j] = spzeros(Int64,is[i],is[j])
                for n in 1:N
                    N_sl[i,j][ii[n,i], ii[n,j]] += 1
                end  
            end
        end
        
        return new(y,ii,N,K, is,N_s, mean_y_s, N_sl)
    end
end

# i create a mutable struct chain, containing all the stuff that gets updated every itervation
mutable struct iter_value
    a::Array{Array{Float64,1},1}
    mu::Float64
    tau::Array{Float64,1}
    tau_e::Float64
    
    
    function iter_value(is::Array{Int64,1})
        tau =1 ./ (rand(Chisq(3),size(is,1)).^2)
        tau_e = 1 / (rand(Chisq(3),1)[1]^2)
    
        mu= rand(Normal(0,1), 1)[1]
        a = [rand(Normal(0,1), is[i]) for i in 1:size(is,1)] 
        
        return new(a,mu,tau, tau_e)
    
    end
end

function MCMC_CrossedEffects(data::MyData,T::Int64, collapsed=true, PX=false)
    
    # ### INPUT #######
    # y  : vector of data of length N. 
    # ii : DataFrame Nxk. values of categorical factors (each row is a datapoint). Factors are ordered integers starting at 1
    # T: number of MCMC iterations
    # collapsed: logical flag. Should Gibbs or collapsed gibbs be used?
    # PX: logical flag. Should parameter expansion be used?

    # ### OUTPUT #######
    # samples of \bar{a} and \tau

    
    @extract data: y ii N K is N_s mean_y_s N_sl

    ###### parameters #######
    
    
    val = iter_value(is)
    a_means = [sum(val.a[k] .* [N_s[k][t] for t in 1:is[k]])/N for k in 1:K]
    ##### Vectros and matrices to be output ######
    mu_chain = zeros(Float64, T)
    a_means_chain = Array{Float64}(undef, K, T)
    tau_chain = Array{Float64}(undef, K, T)
    tau_e_chain = zeros(Float64, T)
    SS0 = 0
    
    #### Sampling 
    msg = ""
    for t in 1:T
        print("\r", " "^length(msg),"\r")
        msg="t$t"
        print(msg)
        
        a_means_chain[:,t] .= a_means 
        tau_chain[:,t] .= val.tau
        mu_chain[t] = val.mu
        tau_e_chain[t] = val.tau_e
        pred = zeros(Float64, N)

        if !collapsed ## update mu given a^(1:k)
            mu_mean = mean_y - sum(a_means)
            mu_var = 1/(N*val.tau_e)
            val.mu = rand(Normal(mu_mean, sqrt(mu_var)), 1)[1]

        end

        for k in 1:K
            s_k = N_s[k].*val.tau_e ./ (val.tau[k].+ N_s[k]*val.tau_e)

            sum_a_l =  sum_on_all(val.a,N_sl, is,k, N_s)
            #end
            if collapsed
                mu_mean = sum(s_k .* (mean_y_s[k].- sum_a_l))/sum(s_k)[1]
                var_mean = 1/(val.tau[k]*sum(s_k))
                val.mu = rand(Normal(mu_mean, sqrt(var_mean)), 1)[1]
            end
            
            a_k_mean = val.tau_e .* N_s[k]./(val.tau_e.*N_s[k] .+ val.tau[k]).*(mean_y_s[k].-val.mu.-sum_a_l)
            a_k_var = 1 ./(N_s[k].*val.tau_e .+ val.tau[k])
            val.a[k] = [rand(Normal(a_k_mean[i], sqrt(a_k_var[i])))[1] for i in 1:is[k]] #
            a_means[k] = sum(val.a[k] .* N_s[k])/N 

            if PX
                prec_alpha_k = val.tau_e*sum(N_s[k] .* val.a[k].^2)
                mean_alpha_k = sum(val.a[k] .* (N_s[k] .* val.tau_e .+ val.tau[k]) .*  a_k_mean) / prec_alpha_k
                
                alpha_k = rand(Normal(mean_alpha_k, 1/sqrt(prec_alpha_k)),1)
                #println(alpha_k)
                val.a[k] = val.a[k] .* alpha_k
            end 
            #println(sum(val.a[k].^2))
            val.tau[k] = rand(Gamma(size(val.a[k],1)/2 -0.5, 2/sum(val.a[k].^2)),1)[1]        
            #println(val.tau_e)
            pred .+= val.a[k][ii[:,k]]
        end 
       
        pred .+= val.mu
        SS0 = sum((y .- pred).^2)

        val.tau_e = rand(Gamma(N/2-0.5, 2/SS0),1)[1]
    end 
        
    println(SS0/N)    
    return val, a_means_chain, tau_chain, tau_e_chain, mu_chain
end

function maximal_coupling_normal(mu_x::Float64,mu_y::Float64, var_x::Float64, var_y::Float64)
    
    x = rand(Normal(mu_x, sqrt(var_x)))
    w = rand()*pdf.(Normal(mu_x,sqrt(var_x)),x)
    
    if w < pdf.(Normal(mu_y,sqrt(var_y)),x)
        return (x,x)
    else
        while true
            y = rand(Normal(mu_y, sqrt(var_y)))
            w = rand()*pdf.(Normal(mu_y,sqrt(var_y)),y)
            if w > pdf.(Normal(mu_x,sqrt(var_x)),y)
                return (x,y)
            end 
        end
    end
end


function maximal_coupling_gam(a_x::Float64, a_y::Float64,b_x::Float64,b_y::Float64)
    x = rand(Gamma(a_x, b_x),1)[1]
    w = rand()*pdf.(Gamma(a_x,b_x),x)
    
    if w < pdf.(Gamma(a_y,b_y),x)
        return (x,x)
    else
        while true
            y = rand(Gamma(a_y, b_y),1)[1]
            w = rand()*pdf.(Gamma(a_y,b_y),y)
            if w > pdf.(Gamma(a_x,b_x),y)
                return (x,y)
            end 
        end
    end
end


function MCMC_CrossedEffects_coupled(data::MyData,T::Int64, collapsed=true, PX=false)

    # need to select a burn in period, and then implement the estimator, remembering that 
    
    @extract data: y ii N K is N_s mean_y_s N_sl

    
    val_1 = iter_value(is)
    val_2 = iter_value(is)
    ###### parameters #######
    a_means_1 = [sum(val_1.a[k] .* [N_s[k][t] for t in 1:is[k]])/N for k in 1:K]
    a_means_2 = [sum(val_2.a[k] .* [N_s[k][t] for t in 1:is[k]])/N for k in 1:K]
    ##### Vectros and matrices to be output ######
    mu_chain_1 = zeros(Float64, T)
    a_means_chain_1 = Array{Float64}(undef, K, T)
    tau_chain_1 = Array{Float64}(undef, K, T)
    tau_e_chain_1 = zeros(Float64, T)
    a_chain_1 = Array{ Array{Array{Float64,1},1},1}(undef,T)
    
    mu_chain_2 = zeros(Float64, T)
    a_means_chain_2 = Array{Float64}(undef, K, T)
    tau_chain_2 = Array{Float64}(undef, K, T)
    tau_e_chain_2 = zeros(Float64, T)
    a_chain_2 = Array{ Array{Array{Float64,1},1},1}(undef,T)
    
    
    SS0_1 = 0
    SS0_2 = 0
    
    finished = false
    
    #### Sampling 
    msg = ""
    
    for t in 1:T
        print("\r", " "^length(msg),"\r")
        msg="t$t"
        print(msg)
        
        a_means_chain_1[:,t] .= a_means_1
        tau_chain_1[:,t] .= val_1.tau
        mu_chain_1[t] = val_1.mu
        tau_e_chain_1[t] = val_1.tau_e
        a_chain_1[t] = val_1.a
        
        
        a_means_chain_2[:,t] .= a_means_2
        tau_chain_2[:,t] .= val_2.tau
        mu_chain_2[t] = val_2.mu
        tau_e_chain_2[t] = val_2.tau_e
        a_chain_2[t] = val_2.a
        
        
        pred_1 = zeros(Float64, N)
        pred_2 = zeros(Float64, N)

        if !collapsed ## update mu given a^(1:k)
            mu_mean_1 = mean_y - sum(a_means_1)
            mu_var_1 = 1/(N*val_1.tau_e)
            
            
            mu_mean_2 = mean_y - sum(a_means_2)
            mu_var_2 = 1/(N*val_2.tau_e)
            val_1.mu, val_2.mu = maximal_coupling_normal(mu_mean_1, mu_mean_2, var_mean_1, var_mean_2)

        end

            
        #n_jk = N_s[k]
        for k in 1:K
            s_k_1 = N_s[k].*val_1.tau_e ./ (val_1.tau[k].+ N_s[k]*val_1.tau_e)
            s_k_2 = N_s[k].*val_2.tau_e ./ (val_2.tau[k].+ N_s[k]*val_2.tau_e)
    
        
            sum_a_l_1 =  sum_on_all(val_1.a,N_sl, is,k, N_s)
            sum_a_l_2 =  sum_on_all(val_2.a,N_sl, is,k, N_s)
            #end
            if collapsed
                if val_1.mu != val_2.mu
                    mu_mean_1 = sum(s_k_1 .* (mean_y_s[k].- sum_a_l_1))/sum(s_k_1)[1]
                    var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                    mu_mean_2 = sum(s_k_2 .* (mean_y_s[k].- sum_a_l_2))/sum(s_k_2)[1]
                    var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))


                    val_1.mu, val_2.mu = maximal_coupling_normal(mu_mean_1, mu_mean_2, var_mean_1, var_mean_2)     
                end
            end

            a_k_mean_1 = val_1.tau_e .* N_s[k]./(val_1.tau_e.*N_s[k] .+ val_1.tau[k]).*(mean_y_s[k].-val_1.mu.-sum_a_l_1)
            a_k_var_1 = 1 ./(N_s[k].*val_1.tau_e .+ val_1.tau[k])
            a_k_mean_2 = val_2.tau_e .* N_s[k]./(val_2.tau_e.*N_s[k] .+ val_2.tau[k]).*(mean_y_s[k].-val_2.mu.-sum_a_l_2)
            a_k_var_2 = 1 ./(N_s[k].*val_2.tau_e .+ val_2.tau[k])
            

            aux = [val_1.a[k][i] == val_2.a[k][i] ? (val_1.a[k][i],val_1.a[k][i])  : maximal_coupling_normal(a_k_mean_1[i], a_k_mean_2[i],  a_k_var_1[i],  a_k_var_2[i]) for i in 1:is[k]] #
            val_1.a[k], val_2.a[k] = getindex.(aux,1), getindex.(aux,2)
            #a_means[k] = sum(val.a[k] .* N_s[k])/N 

            if PX
                prec_alpha_k_1 = val_1.tau_e*sum(N_s[k] .* val_1.a[k].^2)
                mean_alpha_k_1 = sum(val_1.a[k] .* (N_s[k] .* val_1.tau_e .+ val_1.tau[k]) .*  a_k_mean_1) / prec_alpha_k_1
                prec_alpha_k_2 = val_2.tau_e*sum(N_s[k] .* val_2.a[k].^2)
                mean_alpha_k_2 = sum(val_2.a[k] .* (N_s[k] .* val_2.tau_e .+ val_2.tau[k]) .*  a_k_mean_2) / prec_alpha_k_2
                
                if alpha_k_1 == alpha_k_2
                    val_1.a[k] = val_1.a[k] .* alpha_k_1
                    val_2.a[k] = val_2.a[k] .* alpha_k_2
                end 
                alpha_k_1, alpha_k_2 = maximal_coupling_normal(mean_alpha_k_1, mean_alpha_k_2, 1/(prec_alpha_k_1), 1/(prec_alpha_k_2)) 
                val_1.a[k] = val_1.a[k] .* alpha_k_1
                val_2.a[k] = val_2.a[k] .* alpha_k_2
            end 
                
            if  val_1.tau[k] != val_2.tau[k]
                val_1.tau[k], val_2.tau[k] = maximal_coupling_gam(size(val_1.a[k],1)/2-0.5, size(val_1.a[k],1)/2-0.5,2/sum(val_1.a[k].^2), 2/sum(val_2.a[k].^2)) 
            end
            pred_1 .+= val_1.a[k][ii[:,k]]
            pred_2 .+= val_2.a[k][ii[:,k]]
        end 
       
        pred_1 .+= val_1.mu
        pred_2 .+= val_2.mu
        
        SS0_1 = sum((y .- pred_1).^2)
        SS0_2 = sum((y .- pred_2).^2)
        if val_1.tau_e != val_2.tau_e
            val_1.tau_e, val_2.tau_e = maximal_coupling_gam(N/2-0.5, N/2-0.5, 2/SS0_1, 2/SS0_2)

        end

        if val_1.mu == val_2.mu &&  val_1.tau_e == val_2.tau_e  && val_1.a == val_2.a && val_1.tau == val_2.tau 
            finished = true
            println("yeeee \t",t)
            break;
        end
    end   
    
    if finished
        mu = mu_chain_1[10]
        ind = 10
        while mu_chain_1[ind]!= mu_chain_2[ind-1] && ind<T
            mu+= mu_chain_1[ind]-mu_chain_2[ind-1]
            ind +=1
        end

        tau_e = tau_e_chain_1[10]
        ind = 10
        while tau_e_chain_1[ind]!= tau_e_chain_2[ind-1] && ind<T
            tau_e+= tau_e_chain_1[ind]-tau_e_chain_2[ind-1]
            ind+=1
        end

        a = a_chain_1[10]
        for k in 1:K
            for j in 1:is[k]
                ind = 10
                while a_chain_1[ind][k][j]!= a_chain_2[ind-1][k][j] && ind<T

                    a[k][j] += a_chain_1[ind][k][j]-a_chain_2[ind-1][k][j]
                    ind+=1
                end
            end
        end

        tau = tau_chain_1[:,10]
        for k in 1:K
            ind = 10
            while tau_chain_1[k,ind]!= tau_chain_2[k, ind-1] && ind<T
                tau[k] +=  tau_chain_1[k, ind]- tau_chain_2[k,ind-1]
                ind+=1
            end

        end
        return mu, tau_e, a, tau
    else
        return -1
        
    end

    # sta roba me la calcolo e passo solo la stima  return mu_chain_1, mu_chain_2, a_chain_1, a_chain_2, tau_e_chain_1, tau_e_chain_2, tau_chain_1, tau_chain_2
end









        
    
    