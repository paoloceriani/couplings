###### DEFINE FUNCTION WITH MCMC SAMPLERS ######
#####

Using Statistics 

function MCMC_CrossedEffects(y,ii,T=1000, collapsed=True, PX= FALSE)

require(Matrix)
MCMC_CrossedEffects<-function(y,ii,T=1000,collapsed=TRUE,PX=FALSE){
  # ### INPUT #######
  # y  : vector of data of length N. 
  # ii : matrix Nxk. values of categorical factors (each row is a datapoint). Factors are ordered integers starting at 1
  # T: number of MCMC iterations
  # collapsed: logical flag. Should Gibbs or collapsed gibbs be used?
  # PX: logical flag. Should parameter expansion be used?

  # ### OUTPUT #######
  # samples of \bar{a} and \tau
  
  ####  PRECOMPUTING SOME NECESSARY OBJECTS ########
  N<-length(y)# length of data
  k<-dim(ii)[2]# number of factors
  nn<-apply(ii,2,max)
  mean_y<-mean(y)
  mean_y_s<-lapply(c(1:k),function(s){aggregate(y,by=list(ii[,s]),mean)$x})
  #### CREATE A LIST OF VECTORS WITH DATA COUNTS n^{(k)}_{i_k}
  N_s<-lapply(c(1:k),function(s){table(ii[,s])})
  #### CREATE A LIST OF MATRICES WITH CROSS COUNTS n^{(k,s)}_{i_k,i_s}
  N_sl<-matrix(list(), nrow=k, ncol=k)
  for (s in 1:k){
    for (l in c(1:k)[-s]){
      N_sl[[s,l]]<-Matrix(c(table(ii[,s],ii[,l])), nrow = nn[s], ncol = nn[l], sparse = TRUE)
    }
  }
  
  ######## RANDOMLY INITIALIZE PARAMETERS
  tau<-1/(rchisq(n = k,df = 3)^2)
  tau_e<-1/(rchisq(n = 1,df = 3)^2)
  mu<-rnorm(1)
  a<-lapply(nn,rnorm)
  a_means<-vapply(c(1:k),function(s){sum(a[[s]]*N_s[[s]])/N},FUN.VALUE = 1)
  
  ######## VECTORS AND MATRICES TO BE OUTPUT
  mu_chain<-rep(NA,T)
  a_means_chain<-matrix(rep(NA,T*k),nrow = k,ncol = T)
  tau_chain<-matrix(rep(NA,T*k),nrow = k,ncol = T)
  tau_e_chain<-rep(NA,T)
  
  ##### START SAMPLING
  for(t in 1:T){
    mu_chain[t]<-mu
    a_means_chain[,t]<-a_means
    tau_e_chain[t]<-tau_e
    tau_chain[,t]<-tau
    predictor<-0
    if(!collapsed){## update mu given a^(1:k)
      mu_mean<-mean_y-sum(a_means)
      mu_var<-1/(N*tau_e)
      mu<-rnorm(n = 1,mean = mu_mean,sd = sqrt(mu_var))
    }
    for (s in 1:k){
      
      ## UPDATE MEAN 
      # compute sum_a_l_given_s. It is a vector of length nn[s]
      vec<-0
      for(l in c(1:k)[-s]){vec<-vec+N_sl[[s,l]]%*% matrix(a[[l]],ncol=1)}
      sum_a_l_given_s<-as.vector(vec)/as.vector(N_s[[s]])
      if(collapsed){# update mu given a^(1:k\s)
        mu_mean<-sum(tau_e*N_s[[s]]/(tau_e*N_s[[s]]+tau[s])*(
          mean_y_s[[s]]-sum_a_l_given_s))/sum( tau_e*N_s[[s]]/(tau_e*N_s[[s]]+tau[s]))
        mu_var<-1/(tau[s]*sum(tau_e*N_s[[s]]/(tau_e*N_s[[s]]+tau[s])))
        mu<-rnorm(n = 1,mean = mu_mean,sd = sqrt(mu_var))
      }

      ## update a^(s) given mu and a^(1:k\s)
      a_s_mean<-tau_e*N_s[[s]]/(tau_e*N_s[[s]]+tau[s])*(mean_y_s[[s]]-mu-sum_a_l_given_s)
      a_s_var<-1/(tau_e*N_s[[s]]+tau[s])
      a[[s]]<-rnorm(n = nn[s],mean = a_s_mean,sd = sqrt(a_s_var))
      a_means[s]<-sum(a[[s]]*N_s[[s]])/N # store for output
      predictor<-predictor+a[[s]][ii[,s]] # update predictor to compute SS0
      
      ## UPDATE PRECISION tau_s (USING FLAT PRIOR)
      if (PX){# perform PX-DA update
        prec_alpha_s<-tau_e*sum(N_s[[s]]*a[[s]]^2)
        mean_alpha_s<-sum(a[[s]]*(N_s[[s]]*tau_e+tau[s])*a_s_mean)/prec_alpha_s
        alpha_s<-rnorm(1,mean_alpha_s,sd = 1/sqrt(prec_alpha_s))
        a[[s]]<-a[[s]]*alpha_s  # alpha_s is an auxiliary parameter
      }
      # update precision tau_s given the rest. Assume flat prior on 1/sqrt(tau_k)
      tau[s]<-rgamma(n = 1,shape = length(a[[s]])/2 -0.5 ,rate = sum(a[[s]]^2)/2)
    }
    # update precision tau_e given the rest
    predictor<-predictor+mu # update predictor needed to compute SS0
    SS0<-sum((y-predictor)^2) # compute SS_0
    tau_e<-rgamma(n = 1,shape = N/2-0.5,rate = SS0/2)
  }
  return(list(mu_chain=mu_chain,a_means_chain=a_means_chain,
              tau_chain=tau_chain,tau_e_chain=tau_e_chain))
}

###### LOAD AND PRE-PROCESS INSTEVAL DATASET ######
#####
require("lme4")
y<-InstEval$y
factors<-matrix(as.numeric(as.matrix(InstEval[,c(1:6)])),ncol=6)
# CHOOSE WHICH FACTOR TO INCLUDE IN THE ANALYSIS
factor.set<-c(1:4,6)
k<-length(factor.set)
# RESCALE TO MAKE FACTORS FROM 1 TO n_k WITH NO GAPS AND COMPUTE SOME PARAMETERS
convert_factors_to_sorted_integers<-function(v){
  values<-unique(v)
  return(vapply(c(1:length(v)),function(i){which(values==v[i])},FUN.VALUE = 1))
}
ii<-apply(X = factors[,factor.set],FUN = convert_factors_to_sorted_integers,MARGIN = 2)
nn<-apply(ii,2,max)
N<-dim(ii)[1]

###### RUN MCMC SAMPLERS ######
#####
# SET NUMBER OF ITERATIONS
T<-1000
print("run standard gibbs")
print(time_GS<-system.time(
  output_GS<-MCMC_CrossedEffects(y=y,ii=ii,T=T,collapsed=FALSE,PX=FALSE)
)[1])
print("run collapsed gibbs")
print(time_cGS<-system.time(
  output_cGS<-MCMC_CrossedEffects(y=y,ii=ii,T=T,collapsed=TRUE,PX=FALSE)
)[1])
print("run standard gibbs + pxda")
print(time_GS_PXDA<-system.time(
  output_GS_PXDA<-MCMC_CrossedEffects(y=y,ii=ii,T=T,collapsed=FALSE,PX=TRUE)
)[1])
print("run collapsed gibbs + pxda")
print(time_cGS_PXDA<-system.time(
  output_cGS_PXDA<-MCMC_CrossedEffects(y=y,ii=ii,T=T,collapsed=TRUE,PX=TRUE)
)[1])

####### PLOT TRACEPLOTS #########
#####
par(mfrow=c(2,1+k))
par(mar=c(4,2,4.1,1))
par(mgp=c(1.1,0.2,0))
plot(output_GS$mu_chain,type="l")
title("output_GS")
for(s in c(1:k)){
  plot(output_GS$a_means_chain[s,],type="l")
}
plot(output_GS$tau_e_chain,type="l")
for(s in c(1:k)){
  plot(output_GS$tau_chain[s,],type="l")
}
par(mfrow=c(1,1))

par(mfrow=c(2,1+k))
plot(output_cGS$mu_chain,type="l")
title("output_cGS")
for(s in c(1:k)){
  plot(output_cGS$a_means_chain[s,],type="l")
}
plot(output_cGS$tau_e_chain,type="l")
for(s in c(1:k)){
  plot(output_cGS$tau_chain[s,],type="l")
}
par(mfrow=c(1,1))

par(mfrow=c(2,1+k))
plot(output_GS_PXDA$mu_chain,type="l")
title("output_GS_PXDA")
for(s in c(1:k)){
  plot(output_GS_PXDA$a_means_chain[s,],type="l")
}
plot(output_GS_PXDA$tau_e_chain,type="l")
for(s in c(1:k)){
  plot(output_GS_PXDA$tau_chain[s,],type="l")
}
par(mfrow=c(1,1))

par(mfrow=c(2,1+k))
plot(output_cGS_PXDA$mu_chain,type="l")
title("output_cGS_PXDA")
for(s in c(1:k)){
  plot(output_cGS_PXDA$a_means_chain[s,],type="l")
}
plot(output_cGS_PXDA$tau_e_chain,type="l")
for(s in c(1:k)){
  plot(output_cGS_PXDA$tau_chain[s,],type="l")
}
par(mfrow=c(1,1))
par(mar=c(5.1,4.1,4.1,2.1))
par(mgp=c(3,1,0))

