import numpy as np
import scipy as sp
import scipy.stats
import numpy.random as random
import matplotlib.pyplot as plt
import pandas as pd
import random

def countmemb(itr):
    d = np.zeros(np.max(itr))

    for val in itr:
        d[val-1] += 1

    return d


def sum_on_all(a, N_sl, k, N_s, iss):
    #limitiamo il numero di liste 
    K = len(a)
    
    res = np.zeros(iss[k])
    kr = np.array(range(K))
    for l in kr[kr!= k]:
        res += np.matmul(N_sl[k][l],a[l])
    
    for j in range(iss[k]):
        if N_s[k][j] != 0:
            res[j] = res[j]/ N_s[k][j]
        else:
            res[j] = 0

    return res


def distance(val_1, val_2):
    aux = sum([sum(np.power(val_1.a[k]-val_1.a[k],2)) for k in range(len(val_1.a))]) + (val_1.mu-val_2.mu)**2 + (val_1.tau_e-val_2.tau_e)**2+sum(np.power(val_1.tau-val_2.tau,2))
    
    if aux!= 0:
        dist = -np.log(aux)
    else:
        dist = 50
 
    return dist


class Data_sim:
    def __init__(self,N,K,I,mu):
        self.K = K
        self.I = I
        self.N = N
        self.mu = mu
        xk =  1+np.arange(I)
        pk = [1/I for i in range(I)]
        
        custm = sp.stats.rv_discrete(name='custm', values=(xk, pk))
        self.ii = custm.rvs(size=(N,K)) 
        self.y = np.array([sum(self.ii[i,:])+mu for i in range(N)])
        self.iss = [np.max(self.ii[:,k]) for k in range(K)] 
        self.mean_y = np.mean(self.y)
        self.mean_y_s = np.array([[np.mean(self.y[self.ii[:,k] == h]) if not np.isnan(np.mean(self.y[self.ii[:,k] == h])) else 0 for h in range(1,self.iss[k]+1)] for k in range(self.K) ])
        self.N_s = np.array([countmemb(self.ii[:,k]) for k in range(self.K)])
        self.N_sl = [[ np.zeros((self.iss[i],self.iss[j])) for j in range(self.K)] for i in range(self.K)]
        for i in range(self.K) :
            for j in range(self.K):
                for n in range(self.N):
                    self.N_sl[i][j][self.ii[n,i]-1, self.ii[n,j]-1] += 1
        
        


class Data_df:
    def __init__(self,df):
        #try with real dataset 
        self.y = df['y']
        self.ii = np.array(df[['s','d']])
        self.I = self.ii.max()
        self.K = self.ii.shape[1]
        self.N= self.ii.shape[0]
        self.iss = [np.max(self.ii[:,k]) for k in range(self.K)] 
        self.mean_y = np.mean(self.y)
        self.mean_y_s = np.array([[np.mean(self.y[self.ii[:,k] == h]) if not np.isnan(np.mean(self.y[self.ii[:,k] == h])) else 0 for h in range(1,self.iss[k]+1)] for k in range(self.K) ])
        self.N_s = np.array([countmemb(self.ii[:,k]) for k in range(self.K)])
        self.N_sl = [[ np.zeros((self.iss[i],self.iss[j])) for j in range(self.K)] for i in range(self.K)]
        for i in range(self.K) :
            for j in range(self.K):
                for n in range(self.N):
                    self.N_sl[i][j][self.ii[n,i]-1, self.ii[n,j]-1] += 1
            
class iter_value_r:    
    def __init__(self, I,K,iss):
        self.tau = np.ones(K)*np.random.rand()
        self.tau_e = np.random.rand()
        self.mu = np.random.normal(0,1)
        self.a = [np.random.normal(0,1/self.tau[k],size=iss[k]) for k in range(K)]

class iter_value:    
    def __init__(self, I,K,iss):
        self.tau = np.ones(K)*0.5
        self.tau_e = 0.5
        self.mu = np.random.normal(0,1)
        self.a = [np.random.normal(0,1/self.tau[k],size=iss[k]) for k in range(K)]


def MCMC_sampler(data,T, collapsed, PX):
    
    N = data.N
    K = data.K
    val_1 = iter_value(data.I,data.K, data.iss)
    val_2 = iter_value(data.I,data.K, data.iss)

    a_means_1 = [sum(val_1.a[k] * data.N_s[k])/N for k in range(data.K)]
    a_means_2 = [sum(val_2.a[k] * data.N_s[k])/N for k in range(data.K)]
    mu_chain_1 = []
    mu_chain_2 = []
    a_means_chain_1 = np.zeros(shape=(data.K,T))
    a_means_chain_2 = np.zeros(shape=(data.K,T))
    SS0_chain_1 = []
    SS0_chain_2 = []
    dist = []
    t_fin = 0
    
    for t in range(T):
        print(t, end='\r')

        mu_chain_1.append(val_1.mu)
        mu_chain_2.append(val_2.mu)
        a_means_chain_1[:,t]=a_means_1
        a_means_chain_2[:,t]=a_means_2
        pred_1 = 0
        pred_2 = 0

        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            s_k_2 = data.N_s[k]*val_2.tau_e / (val_2.tau[k]+ data.N_s[k]*val_2.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            sum_a_l_2 =  sum_on_all(val_2.a,data.N_sl,k, data.N_s,data.iss)
            #print(sum_a_l_1,sum_a_l_2 )

            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                mu_mean_2= sum( s_k_2 *(data.mean_y_s[k] - sum_a_l_2))/sum(s_k_2)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))

                aux = np.random.normal(0,1)
                val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                val_2.mu = mu_mean_2+aux*np.sqrt(var_mean_2)
                
           
            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  

            a_k_mean_2 = val_2.tau_e*data.N_s[k]/(val_2.tau_e*data.N_s[k]+val_2.tau[k])*(np.array(data.mean_y_s[k])-val_2.mu-sum_a_l_2)
            a_k_var_2 = 1/(data.N_s[k]*val_2.tau_e+val_2.tau[k])  

            aux = np.random.normal(0,1, size= data.iss[k])

            #print(k,"\t",a_k_mean_1+ np.sqrt(a_k_var_1)*aux)
            val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
            val_2.a[k]= a_k_mean_2+ np.sqrt(a_k_var_2)*aux

            a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N
            a_means_2[k] = sum(val_2.a[k]*data.N_s[k])/N
            #print(k, "\t", a_means_1[k],"\t",a_means_2[k] )


            if PX:
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1

                prec_alpha_k_2= val_2.tau_e*sum(data.N_s[k]*np.power(val_2.a[k],2))
                mean_alpha_k_2 = sum(val_2.a[k]*(data.N_s[k]*val_2.tau_e+val_2.tau[k])*a_k_mean_2)/prec_alpha_k_2

                aux = np.random.normal(0,1, size= 1)

                alpha_k_1 = mean_alpha_k_1+ 1/np.sqrt(prec_alpha_k_1)*aux
                alpha_k_2 = mean_alpha_k_2+ 1/np.sqrt(prec_alpha_k_2)*aux
                val_1.a[k] = val_1.a[k]*alpha_k_1
                val_2.a[k] = val_2.a[k]*alpha_k_2



            pred_1 += val_1.a[k][data.ii[:,k]-1]
            pred_2 += val_2.a[k][data.ii[:,k]-1]
    #        val_1.tau[k] = random.gamma(size=1, shape= I/2-0.5, scale= 2/sum(np.power(val_1.a[:,k],2)))
        #print(a_means)
        pred_1 += val_1.mu
        pred_2 += val_2.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        SS0_2 = sum(np.power((data.y-pred_2),2))
        if (val_1.mu == val_2.mu) and np.array([(val_1.a[k] == val_2.a[k]).all() for k in range(data.K)]).all() :
            print("yeeeee, ",t)

            break

    #    val_1.tau_e = random.gamma(size=1, shape= N/2-0.5, scale=2/SS0)

        SS0_chain_1.append(SS0_1)
        SS0_chain_2.append(SS0_2)
        dist.append( distance(val_1, val_2))
    t_fin = t

    print(SS0_1/N)
    print(SS0_2/N)
    
    return mu_chain_1, mu_chain_2, dist,t_fin


def maximal_coupling_normal(mu_x, mu_y, var_x, var_y):
    x = np.random.normal(mu_x, np.sqrt(var_x))
    w = np.random.rand()*normal_dens(mu_x,np.sqrt(var_x),x)
    
    if w < normal_dens(mu_y,np.sqrt(var_y),x):
        return (x,x)
    else:
        while True:
            y = np.random.normal(mu_y, np.sqrt(var_y))
            w = np.random.rand()*normal_dens(mu_y,np.sqrt(var_y),y)
            if w > normal_dens(mu_x,np.sqrt(var_x),y):
                return (x,y)
            
def maximal_coupling_gamma(a_x,a_y, b_x, b_y):
    x = np.random.gamma(a_x, b_x)
    w = np.random.rand()
    
    if w < np.power(b_x/b_y,a_y)*np.exp(x*(1/b_x-1/b_y)):
        return (x,x)
    else:
        while True:
            y = np.random.gamma(a_y, b_y)
            w = np.random.rand()
            if w > np.power(b_y/b_x,a_x)*np.exp(y*(1/b_y-1/b_x)):
                return (x,y)

def normal_dens(mu,sigma,x):
    return 1/np.power(2*np.pi*sigma**2,0.5)*np.exp(-0.5*np.power(x-mu,2)/np.power(sigma,2))




def MCMC_sampler_var(data,T, collapsed, PX):
    
    N = data.N
    K = data.K
    val_1 = iter_value_r(data.I,data.K, data.iss)
    val_2 = iter_value_r(data.I,data.K, data.iss)

    a_means_1 = [sum(val_1.a[k] * data.N_s[k])/N for k in range(data.K)]
    a_means_2 = [sum(val_2.a[k] * data.N_s[k])/N for k in range(data.K)]
    mu_chain_1 = []
    mu_chain_2 = []
    tau_chain_1= np.zeros((K,T))
    tau_chain_2= np.zeros((K,T))
    tau_e_1 = []
    tau_e_2 = []
    a_means_chain_1 = np.zeros(shape=(data.K,T))
    a_means_chain_2 = np.zeros(shape=(data.K,T))
    SS0_chain_1 = []
    SS0_chain_2 = []
    dist = []
    t_fin = 0
    
    for t in range(T):
        print(t, end='\r')

        mu_chain_1.append(val_1.mu)
        mu_chain_2.append(val_2.mu)
        a_means_chain_1[:,t]=a_means_1
        a_means_chain_2[:,t]=a_means_2
        tau_chain_1[:,t] = val_1.tau
        tau_chain_2[:,t] = val_2.tau
        tau_e_1.append(val_1.tau_e)
        tau_e_2.append(val_2.tau_e)
        
        
        
        pred_1 = 0
        pred_2 = 0

        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            s_k_2 = data.N_s[k]*val_2.tau_e / (val_2.tau[k]+ data.N_s[k]*val_2.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            sum_a_l_2 =  sum_on_all(val_2.a,data.N_sl,k, data.N_s,data.iss)
            #print(sum_a_l_1,sum_a_l_2 )

            if collapsed:
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                mu_mean_2= sum( s_k_2 *(data.mean_y_s[k] - sum_a_l_2))/sum(s_k_2)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))

                aux = np.random.normal(0,1)
                val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                val_2.mu = mu_mean_2+aux*np.sqrt(var_mean_2)
                
           
            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  

            a_k_mean_2 = val_2.tau_e*data.N_s[k]/(val_2.tau_e*data.N_s[k]+val_2.tau[k])*(np.array(data.mean_y_s[k])-val_2.mu-sum_a_l_2)
            a_k_var_2 = 1/(data.N_s[k]*val_2.tau_e+val_2.tau[k])  

            aux = np.random.normal(0,1, size= data.iss[k])

            #print(k,"\t",a_k_mean_1+ np.sqrt(a_k_var_1)*aux)
            val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
            val_2.a[k]= a_k_mean_2+ np.sqrt(a_k_var_2)*aux

            a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N
            a_means_2[k] = sum(val_2.a[k]*data.N_s[k])/N
            #print(k, "\t", a_means_1[k],"\t",a_means_2[k] )


            if PX:
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1

                prec_alpha_k_2= val_2.tau_e*sum(data.N_s[k]*np.power(val_2.a[k],2))
                mean_alpha_k_2 = sum(val_2.a[k]*(data.N_s[k]*val_2.tau_e+val_2.tau[k])*a_k_mean_2)/prec_alpha_k_2

                aux = np.random.normal(0,1, size= 1)

                alpha_k_1 = mean_alpha_k_1+ 1/np.sqrt(prec_alpha_k_1)*aux
                alpha_k_2 = mean_alpha_k_2+ 1/np.sqrt(prec_alpha_k_2)*aux
                val_1.a[k] = val_1.a[k]*alpha_k_1
                val_2.a[k] = val_2.a[k]*alpha_k_2



            pred_1 += val_1.a[k][data.ii[:,k]-1]
            pred_2 += val_2.a[k][data.ii[:,k]-1]
            
            np.random.seed(random.randint(1,5000))
            val_1.tau[k] = np.random.gamma(size=1, shape= data.I/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
            val_2.tau[k] = np.random.gamma(size=1, shape= data.I/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
            np.random.seed()
            #print(val_1.tau[k])
        #print(a_means)
        pred_1 += val_1.mu
        pred_2 += val_2.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        SS0_2 = sum(np.power((data.y-pred_2),2))
        if (val_1.mu == val_2.mu) and np.array([(val_1.a[k] == val_2.a[k]).all() for k in range(data.K)]).all() :
            print("yeeeee, ",t)

            break
        np.random.seed(random.randint(1,5000))
        val_1.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_1)
        val_2.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_2)
        np.random.seed()
        SS0_chain_1.append(SS0_1)
        SS0_chain_2.append(SS0_2)
        dist.append( distance(val_1, val_2))
        if SS0_1<1e-1 and SS0_2<1e-1:
            print("too close")
            break
            
    t_fin = t

    print(SS0_1/N)
    print(SS0_2/N)
    
    return mu_chain_1, mu_chain_2, tau_e_1, tau_e_2, tau_chain_1, tau_chain_2, dist,t_fin


def MCMC_sampler_coupling(data,T, collapsed, PX):
    
    N = data.N
    K = data.K
    val_1 = iter_value_r(data.I,data.K, data.iss)
    val_2 = iter_value_r(data.I,data.K, data.iss)

    a_means_1 = [sum(val_1.a[k] * data.N_s[k])/N for k in range(data.K)]
    a_means_2 = [sum(val_2.a[k] * data.N_s[k])/N for k in range(data.K)]
    mu_chain_1 = []
    mu_chain_2 = []
    tau_chain_1= np.zeros((K,T))
    tau_chain_2= np.zeros((K,T))
    tau_e_1 = []
    tau_e_2 = []
    a_means_chain_1 = np.zeros(shape=(data.K,T))
    a_means_chain_2 = np.zeros(shape=(data.K,T))
    SS0_chain_1 = []
    SS0_chain_2 = []
    dist = [distance(val_1,val_2)]
    t_fin = 0
    coupling = False
    for t in range(T):
        print(t, end='\r')

        mu_chain_1.append(val_1.mu)
        mu_chain_2.append(val_2.mu)
        a_means_chain_1[:,t]=a_means_1
        a_means_chain_2[:,t]=a_means_2
        
        tau_chain_1[:,t] = val_1.tau
        tau_chain_2[:,t] = val_2.tau
        tau_e_1.append(val_1.tau_e)
        tau_e_2.append(val_2.tau_e)
        
        
        
        pred_1 = 0
        pred_2 = 0
        #print("inizio",t)
        if dist[t]>3:
            coupling=True
        print(coupling,t)
        
        for k in range(data.K):
            s_k_1 = data.N_s[k]*val_1.tau_e / (val_1.tau[k]+ data.N_s[k]*val_1.tau_e)
            s_k_2 = data.N_s[k]*val_2.tau_e / (val_2.tau[k]+ data.N_s[k]*val_2.tau_e)
            sum_a_l_1 =  sum_on_all(val_1.a,data.N_sl,k, data.N_s,data.iss)
            sum_a_l_2 =  sum_on_all(val_2.a,data.N_sl,k, data.N_s,data.iss)
            #print(sum_a_l_1,sum_a_l_2 )

            if collapsed:
                #print("mu",k, t, coupling)
                mu_mean_1= sum( s_k_1 *(data.mean_y_s[k] - sum_a_l_1))/sum(s_k_1)
                mu_mean_2= sum( s_k_2 *(data.mean_y_s[k] - sum_a_l_2))/sum(s_k_2)
                var_mean_1 = 1/(val_1.tau[k]*sum(s_k_1))
                var_mean_2 = 1/(val_2.tau[k]*sum(s_k_2))
                if not coupling:
                    aux = np.random.normal(0,1)
                    val_1.mu = mu_mean_1+aux*np.sqrt(var_mean_1)
                    val_2.mu = mu_mean_2+aux*np.sqrt(var_mean_2)
                else:
                    val_1.mu, val_2.mu = maximal_coupling_normal(mu_mean_1, mu_mean_2, var_mean_1, var_mean_2)
                    
                
                
           
            a_k_mean_1 = val_1.tau_e*data.N_s[k]/(val_1.tau_e*data.N_s[k]+val_1.tau[k])*(np.array(data.mean_y_s[k])-val_1.mu-sum_a_l_1)
            a_k_var_1 = 1/(data.N_s[k]*val_1.tau_e+val_1.tau[k])  

            a_k_mean_2 = val_2.tau_e*data.N_s[k]/(val_2.tau_e*data.N_s[k]+val_2.tau[k])*(np.array(data.mean_y_s[k])-val_2.mu-sum_a_l_2)
            a_k_var_2 = 1/(data.N_s[k]*val_2.tau_e+val_2.tau[k])  

            if not coupling:
               # print("a",k, t)
                aux = np.random.normal(0,1, size= data.iss[k])
                val_1.a[k]= a_k_mean_1+ np.sqrt(a_k_var_1)*aux
                val_2.a[k]= a_k_mean_2+ np.sqrt(a_k_var_2)*aux
                a_means_1[k] = sum(val_1.a[k]*data.N_s[k])/N
                a_means_2[k] = sum(val_2.a[k]*data.N_s[k])/N
            else:
                for o in range(len(val_1.a[k])):
                    val_1.a[k][o], val_2.a[k][o] = maximal_coupling_normal(a_k_mean_1[o], a_k_mean_2[o], a_k_var_1[o], a_k_var_2[o])
                    


            if PX:
                prec_alpha_k_1= val_1.tau_e*sum(data.N_s[k]*np.power(val_1.a[k],2))
                mean_alpha_k_1 = sum(val_1.a[k]*(data.N_s[k]*val_1.tau_e+val_1.tau[k])*a_k_mean_1)/prec_alpha_k_1

                prec_alpha_k_2= val_2.tau_e*sum(data.N_s[k]*np.power(val_2.a[k],2))
                mean_alpha_k_2 = sum(val_2.a[k]*(data.N_s[k]*val_2.tau_e+val_2.tau[k])*a_k_mean_2)/prec_alpha_k_2

                aux = np.random.normal(0,1, size= 1)

                alpha_k_1 = mean_alpha_k_1+ 1/np.sqrt(prec_alpha_k_1)*aux
                alpha_k_2 = mean_alpha_k_2+ 1/np.sqrt(prec_alpha_k_2)*aux
                val_1.a[k] = val_1.a[k]*alpha_k_1
                val_2.a[k] = val_2.a[k]*alpha_k_2



            pred_1 += val_1.a[k][data.ii[:,k]-1]
            pred_2 += val_2.a[k][data.ii[:,k]-1]

                        
            if not coupling:
                #print("tau",k, t)
                np.random.seed(random.randint(1,5000))
                val_1.tau[k] = np.random.gamma(size=1, shape= data.I/2-0.5, scale= 2/sum(np.power(val_1.a[k],2)))
                val_2.tau[k] = np.random.gamma(size=1, shape= data.I/2-0.5, scale= 2/sum(np.power(val_2.a[k],2)))
                np.random.seed()
                
            else:
                #print("qui",k, t)
                val_1.tau[k], val_2.tau[k] = maximal_coupling_gamma(data.I/2-0.5,data.I/2-0.5,2/sum(np.power(val_1.a[k],2)),2/sum(np.power(val_2.a[k],2)))
            #print(val_1.tau[k])
        #print(a_means)
        #print("pred?",k, t)
        pred_1 += val_1.mu
        pred_2 += val_2.mu
        SS0_1 = sum(np.power((data.y-pred_1),2))
        SS0_2 = sum(np.power((data.y-pred_2),2))
        
            
        if not coupling :
            #print("tau_e",k, t)
            np.random.seed(random.randint(1,5000))
            val_1.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_1)
            val_2.tau_e = np.random.gamma(size=1, shape= N/2-0.5, scale=2/SS0_2)
            np.random.seed()
        else:
            #print("buuu?",k, t)
            val_1.tau_e, val_2.tau_e = maximal_coupling_gamma(N/2-0.5,N/2-0.5,2/SS0_1, 2/SS0_2)
        
        if (val_1.mu == val_2.mu) and np.array([(val_1.a[k] == val_2.a[k]).all() for k in range(data.K)]).all() and val_1.tau_e==val_2.tau_e and np.array(val_1.tau == val_2.tau).all() :
            print("yeeeee, ",t)
            break

            
        SS0_chain_1.append(SS0_1)
        SS0_chain_2.append(SS0_2)
        dist.append( distance(val_1, val_2))
        if SS0_1<1e-3 and SS0_2<1e-3:
            print("too close")
            break
    

    t_fin = t
            
    print(SS0_1/N)
    print(SS0_2/N)
    return mu_chain_1, mu_chain_2, tau_e_1, tau_e_2, tau_chain_1, tau_chain_2, dist,t_fin



